import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AdService} from "../../services/ad.service";
import {FullAd} from "../../models/full-ad.model";

@Component({
  selector: 'app-view-ad',
  templateUrl: './view-ad.component.html',
  styleUrls: ['./view-ad.component.scss']
})
export class ViewAdComponent implements OnInit {

  public ad: FullAd | undefined;

  constructor(private route: ActivatedRoute, private adService: AdService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];

    this.adService.getById(id).subscribe(res => {
      this.ad = res;
    })
  }

}
