import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  readonly form = this.fb.group({
    firstName: ['', [Validators.required]],
    middleName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    phone: ['', [Validators.required]],
    reCaptchaResponse: ['mock', [Validators.required]],
  });

  constructor(
    private fb: FormBuilder,
    private nzMessageService: NzMessageService,
    private nzNotificationService: NzNotificationService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    // Проверяем валидна ли форма
    if (this.form.invalid) {
      this.nzNotificationService.error('Ошибка', 'Форма заполнена неверно');
      Object.values(this.form.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }
    // Если валидна - отправляем запрос, показываем сообщение об успешной регистрации и перенаправляем на вход
    this.auth.register(this.form.getRawValue()).subscribe((res) => {
      this.router.navigateByUrl('/login');
      this.nzNotificationService.success(
        'Успешно!',
        'Регистрация успешно завершена!'
      );
    });
  }
}
