import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  readonly form = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private nzMessageService: NzMessageService,
    private nzNotificationService: NzNotificationService,
    private auth: AuthService,
    private chdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    // Проверяем валидна ли форма
    if (this.form.invalid) {
      this.nzNotificationService.error('Ошибка', 'Форма заполнена неверно');
      Object.values(this.form.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }

    // Если валидна - отправляем запрос, перенаправляем на главную. Добавление токена внутри метода, см. AuthService.login
    this.auth.login(this.form.getRawValue()).subscribe((res) => {
      this.router.navigateByUrl('/');
    });
  }
}
