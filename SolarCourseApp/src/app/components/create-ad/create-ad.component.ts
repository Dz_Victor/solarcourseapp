import { Component, OnInit } from '@angular/core';
import {AdService} from "../../services/ad.service";
import {FormBuilder, Validators} from "@angular/forms";
import {first} from "rxjs";
import {CategoryService} from "../../services/category.service";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {Router} from "@angular/router";
import {NzMessageService} from "ng-zorro-antd/message";
import {PhotoService} from "../../services/photo.service";

@Component({
  selector: 'app-create-ad',
  templateUrl: './create-ad.component.html',
  styleUrls: ['./create-ad.component.scss']
})
export class CreateAdComponent implements OnInit {

  public categories$ = this.categoryService.getAll();

  form = this.fb.group({
    name: [
      '',
      [Validators.required, Validators.maxLength(100)],
    ],
    text: ['', [Validators.maxLength(2000)]],
    photos: [[]],
    categoryId: [null, Validators.required],
    price: [
      0,
      [
        Validators.min(1),
        Validators.pattern(/^\d+(,\d{1,2})?$/),
      ],
    ],
    locationKladrId: [null],
    locationText: ['', Validators.required],
    locationX: [null],
    locationY: [null],
  });
  constructor(private adService: AdService,
              private fb: FormBuilder,
              private nzNotificationService: NzNotificationService,
              private nzMessageService: NzMessageService,
              private router: Router,
              private photoService: PhotoService,
              private categoryService: CategoryService) {

  }

  ngOnInit(): void {
  }

  onSubmit() {
// Проверяем валидна ли форма
    if (this.form.invalid) {
      this.nzNotificationService.error('Ошибка', 'Форма заполнена неверно');
      Object.values(this.form.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return;
    }
    // Если валидна - отправляем запрос, показываем сообщение об успешной регистрации и перенаправляем на вход
    this.adService.createAd(this.form.getRawValue()).subscribe((res: any) => {
      this.router.navigateByUrl('/view/' + res);
      this.nzNotificationService.success(
        'Успешно!',
        'Объявление создано!'
      );
    });
  }

  handleChange(info: any) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      this.nzMessageService.success(`${info.file.name} file uploaded successfully`);
      (this.form.get('photos') as any).patchValue([
        ...this.form.get('photos')!.value as any,
        info.file.response
      ])
    } else if (info.file.status === 'error') {
      this.nzMessageService.error(`${info.file.name} file upload failed.`);
    }
  }
}
