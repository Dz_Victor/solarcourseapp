import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { ru_RU } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import ru from '@angular/common/locales/ru';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import {NzFormModule} from "ng-zorro-antd/form";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzMessageService} from "ng-zorro-antd/message";
import {ErrorInterceptor} from "./services/error.interceptor";
import {AuthInterceptor} from "./services/auth.interceptor";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import { MainComponent } from './components/main/main.component';
import { HeaderComponent } from './components/header/header.component';
import {NzCardModule} from "ng-zorro-antd/card";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import { CreateAdComponent } from './components/create-ad/create-ad.component';
import { ViewAdComponent } from './components/view-ad/view-ad.component';
import {NzCascaderModule} from "ng-zorro-antd/cascader";
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzImageModule} from "ng-zorro-antd/image";

registerLocaleData(ru);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    MainComponent,
    HeaderComponent,
    CreateAdComponent,
    ViewAdComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzFormModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzInputModule,
    NzLayoutModule,
    NzTypographyModule,
    NzCardModule,
    NzSpinModule,
    NzPaginationModule,
    NzCascaderModule,
    NzTreeSelectModule,
    NzInputNumberModule,
    NzUploadModule,
    NzImageModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: ru_RU },
    NzMessageService,
    NzNotificationService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
