import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {GetPagedResult} from "../models/get-paged-result.model";
import {AdGetPageItem} from "../models/ad.model";
import {FullAd} from "../models/full-ad.model";

@Injectable({
  providedIn: 'root'
})
export class AdService {
  private adUrl = `${environment.apiUrl}/advertisements`;

  constructor(private http: HttpClient) { }

  public getPagedFiltered(
    offset: number,
    limit: number
  ): Observable<GetPagedResult<AdGetPageItem>> {
    let url = `${this.adUrl}/getPaged?Offset=${offset}&Limit=${limit}`;
    return this.http.get<GetPagedResult<AdGetPageItem>>(url);
  }

  public updateAds(isFilterChanged = false): void {
    this.getPagedFiltered(
      0,
      10
    ).subscribe((res) => {
    });
  }

  public getById(id: string): Observable<FullAd> {
    return this.http.get<FullAd>(`${this.adUrl}/getById?Id=${id}`);
  }

  public createAd(ad: any): Observable<string> {
    return this.http.post<string>(`${this.adUrl}/create`, ad);
  }
}
