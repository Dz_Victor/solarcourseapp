import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // Берем токен из localStorage
    const token = this.authService.token;

    // Если токен есть
    if (token) {
      // Добавляем его в хедеры запроса
      const tokenizedRequest = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`),
      });
      // Возвращаем модифицированный запрос
      return next.handle(tokenizedRequest);
    }

    // Если токена нет - возвращаем запрос в том виде как есть
    return next.handle(request);
  }
}
