import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { MainComponent } from './components/main/main.component';
import {CreateAdComponent} from "./components/create-ad/create-ad.component";
import {ViewAdComponent} from "./components/view-ad/view-ad.component";
import {AuthGuardGuard} from "./auth-guard.guard";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MainComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegistrationComponent,
  },
  {
    path: 'create-ad',
    component: CreateAdComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'create-ad/:id',
    component: CreateAdComponent,
  },
  {
    path: 'view/:id',
    component: ViewAdComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
